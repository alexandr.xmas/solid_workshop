# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'
require 'pry'
require 'json'
require 'xmlsimple'


class Converter
  def initialize(options, source)
    @options = options
    @source = source
  end

  def convert
    result = File.file?(@source) ? File.read(@source) : raise
    result = if Nokogiri::XML(result).errors.empty?
            XmlSimple.xml_in result
           else
            JSON.parse(result)
           end
    if @options[:reverse]
      result['employees'].reverse!
    end
    if @options[:sort]
      result['employees'].sort_by! { |elem| elem["firstName"]}
    end
    if @options[:format] == 'json'
      p result.to_json
    end
    if @options[:format] == 'xml'
      p XmlSimple.xml_out result
    end
  end
end
