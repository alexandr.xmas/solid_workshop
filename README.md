# solid_workshop

Утилита принимает на вход json или xml, выполняет некоторые манипуляции (если указано) и на выходе выдает также xml или json. В манипуляции входят следующие задачи:

```
Usage: bin/converter [handler]
    -r, --[no-]reverse               Revers items
    -s, --[no-]sort                  Sort items
    -o, --out FORMAT                 Output feed format: json/xml
    -h, --help                       Show help message
    
```

Вывод результата просиходит в STDOUT и в файл `output`. Исходный `input` может быть локальным файлом, так и http адресом по которому нужно его забрать.

## Пример использования:
```
ruby bin/converter --out json -r -s books.xml
ruby bin/converter --out xml books.json
```
